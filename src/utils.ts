export const readFile = (file: File) => new Promise<string | undefined>((resolve, reject) => {
  const reader = new FileReader();

  reader.onload = (e) => resolve(e.target?.result?.toString());
  reader.onerror =  reject;

  reader.readAsText(file);
});
