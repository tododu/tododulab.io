import {array, enums, number, object, optional, string, type Struct} from "superstruct";

export enum Status {
  Pending,
  InProgress,
  Done
}


export type Task = {
  id: number,
  title: string,
  description?: string,
  status: Status,
  depends_on: Array<number>,
  project?: string
}

type TaskValidator = Record<keyof Required<Task>, Struct<any, any>>
const taskValidator: TaskValidator  = {
  id: number(),
  title: string(),
  description: optional(string()),
  status: enums(Object.values(Status) as number[]),
  depends_on: array(number()),
  project: optional(string()),
}

const importValidator = array(object(taskValidator))

export { importValidator }
