import {Dexie, DexieOptions, Table} from "dexie";
import {Status, Task} from "./types.ts";

interface TableDescription<T> extends String {
  phantom?: T
}

type Desc2Table<T> = T extends TableDescription<infer U> ? Table<U> : never;

type DB<T> = {
  [k in keyof T]: Desc2Table<T[k]>
} & Dexie

function createDb<T>(name: string, config: T, version: number = 1, options?: DexieOptions): DB<T> {
  const db = new Dexie(name, options);
  db.version(version).stores(config as any)
  return db as any
}

let db = createDb("tododu", {
  tasks: '++id, title, status, depends_on, project' as TableDescription<Task>
})

export default db;

export const resetDb = async () => {
  await db.tasks.clear()
  await db.tasks.bulkAdd(
    [
      <Task>{
        id: 1,
        title: "move this to done",
        status: Status.InProgress,
        depends_on: [],
        project: "Tour"
      },
      <Task>{
        id: 2,
        title: "now you can tackle this",
        status: Status.Pending,
        depends_on: [1],
        project: "Tour"
      },
      <Task>{
        id: 3,
        title: "and now you got how it works",
        status: Status.Pending,
        depends_on: [2],
        project: "Tour"
      },
    ]
  )
}


